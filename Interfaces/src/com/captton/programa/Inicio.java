package com.captton.programa;

import com.captton.inter.IAnotador;
import com.captton.inter.Lapicera;

import java.util.ArrayList;

import com.captton.inter.Computadora;

public class Inicio {

	public static void main(String[] args) {

		IAnotador l = new Lapicera();
		l.anotar("Hola soy lapicera");
	
		Lapicera lapi = new Lapicera();
		lapi.anotar("desde instancia Lapicera");
		
		IAnotador c = new Computadora();
		c.anotar("Soy compu");
		
		Computadora compu = new Computadora();
		compu.anotar("Desde instancia computadora");
		
		compu.encender();
		
		ArrayList<IAnotador> anotadores = new ArrayList<IAnotador>();
		anotadores.add(l);
		anotadores.add(c);
		
		System.out.println("-------------------");
		for(IAnotador i: anotadores) {
			
			i.anotar("el mensaje");
			
			if (i instanceof Lapicera) {
				
				System.out.println("Es lapicera");
				
			}else {
				
				System.out.println("Es computadora");
				((Computadora)i).encender();
				
			}
			
			
			
			
			
			
			
			
			
		}
		
		
	}

}
